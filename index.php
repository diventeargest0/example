<style>
	#formDone{
		display:none;
	}
	#header{
		text-align:center;
	}
	#nb{
		text-align:right;
	}
	#formDiv{
		position:relative;
		margin:50px auto;   
		min-height:200px;
		width:510px;
		z-index:100;
		padding:30px;
		border:1px solid #383838;   
		background:#D1D1D1;
		background:repeating-linear-gradient(-45deg, #0772ca , #0772ca 30px, #F2F2F2 30px, #F2F2F2 40px, #C2E8F5 40px, #C2E8F5 70px,#F2F2F2 70px, #F2F2F2 80px);
		border-radius:8px;
		box-shadow:0px 1px 6px #3F3F3F;
	}
	#formDiv:after{
		background:#F9F9F9;
		margin:10px;
		position:absolute;
		content :" ";
		bottom:0;
		left:0;
		right:0;
		top:0;
		z-index:-1; 
		border:1px #E5E5E5 solid;       
		border-radius:8px;
	}
	#submit{
    background-color:#eee;
		border-width:0;
		color:#999;
		padding:16px;
		margin-left:20%;
		width:80%;
		cursor:pointer;
		border-radius:50px;
		font-size:20px;
		transition: all 0.3s ease-in;
	}
	#submit:hover{
		color:black;
	}
	#select{
    width:60%;
		background-color:#eee;
		border-width:0;
		color:black;
		font-size:14px;
		padding:10px;
	}
  p .input{
    width:100%;
    background-color:#eee;
		border-width:0;
		color:black;
		font-size:14px;
		padding:16px;
  }
	#childName{
		color:black;
	}
	#childDate{
		color:black;
	}
	#parentName{
		color:black;
	}
	#phone{
		color:black;
	}
	#address{
		color:black;
	}
	#email{
		color:black;
	}
	#button{
		background-color:#0772ca;
		cursor:pointer;
		border:none;
		color:white;
		padding:15px 32px;
		text-align:center;
		text-decoration:none;
		display:inline-block;
		font-size:16px;
	}
	#preloader{
		margin-left:55%;
		position:relative;
		width:59px;
		height:59px;
		background:url('preloader.gif') no-repeat 50% 50%;
	}
	#popupClose {
		cursor: pointer;
		color: #fff;
		position: absolute;
		top: 3px;
		right: 10px;
	}
	.popupHidden {
		display: none;
	}
	.popupText {
		color: #fff;
	}
	.popupVisible {
		animation: fadeIn 1s;
		width: 350px;
		background-color: rgba(0,0,0,.7);
		text-align: center;
		border-radius: 6px;
		padding: 20px 0px;
		position: fixed;
		right: 20px;
		bottom: 20px;
		z-index: 9999;
	}
	@keyframes fadeIn {
		from {opacity: 0;}
		to {opacity: 1;}
	}
</style>
<div id="formDiv">
	<h1 id="header"> Отправить заявку </h1><br>
	<form id="form" action="send.php" target="formDone" method="post" onsubmit="document.getElementById('submited').innerHTML=`<div id='preloader'></div>`;" autocomplete="off">

		<p><select name="select" id="select" required>
			<option value="">Выберите Смену</option>
			<option value="1 СМЕНА с 18 июня по 5 июля">1 СМЕНА с 18 июня по 5 июля</option>
			<option value="2 СМЕНА с 11 по 28 июля">2 СМЕНА с 11 по 28 июля</option>
			<option value="3 СМЕНА с 8 по 25 августа">3 СМЕНА с 8 по 25 августа</option>
		</select></p>

		<p><input class="input" autocomplete="off" type="text" name="childName" id="childName" placeholder="ФИО ребёнка" required> </p>

		<p><input class="input" autocomplete="off" type="text" name="childDate" id="childDate" placeholder="Дата рождения ребёнка" onfocus="(this.type='date')" required> </p>

		<p><input class="input" autocomplete="off" type="text" name="parentName" id="parentName" placeholder="ФИО родителя" required > </p>

		<p><input class="input" autocomplete="off" type="text" name="phone" id="phone" placeholder="Контактный телефон" required> </p>

		<p><input class="input" autocomplete="off" type="text" name="address" id="address" placeholder="Адрес проживания" required> </p>

		<p><input class="input" autocomplete="off" type="email" name="email" id="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Контактный E-mail"> </p>

		<p id="nb"><i>Будьте внимательны</i></p>

		<p id="submited"> <input id="submit" type="submit" value="Отправить"> </p>
	</form>
</div>
<iframe id='formDone' name='formDone'></iframe>
<div class="popupHidden" id="popup">
    <div id="popupClose" onclick="document.getElementById('popup').style.display='none'">✖</div>
    <span class="popupText">Не забывайте проверять почтовый ящик</span>
</div>
