var first_press=false;
document.onkeydown=function(evt){
	evt=evt || window.event;
	if(evt.keyCode==27){
		if(first_press) {
			search(1,``);
			first_press=false;
		}else{
			document.getElementById('searchInput').value='';
			document.getElementById('searchInput').blur();
			var view=document.getElementById('view');
			if(view.style.display=='block'){
				view.style.display='none';
				outLoader();
			}
			first_press=true;
			window.setTimeout(function(){first_press=false;},500);
		}
	}else if(evt.keyCode == 70 && evt.ctrlKey){
		evt.preventDefault();
		document.getElementById('searchInput').focus();
	}
}
function copyToClipboard(self){
	self.id='active';
	var text=self.innerHTML;
    var dummy=document.createElement("textarea");
    document.body.appendChild(dummy);
    dummy.value=text;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
	setTimeout(function(){
		self.removeAttribute('id');
	},1000);
}
function removeFrame(){
var hiddenFrame=document.querySelector('.hiddenFrame');
	if(hiddenFrame!=null){
		hiddenFrame.remove();
	}
}
function view(id){
	showLoader();
	removeFrame();
	var view=document.createElement('iframe');
	view.classList='hiddenFrame';
	view.src='view.php?id='+id;
	view.style.display='none';
	document.body.appendChild(view);
}
function search(page,search){
	showLoader();
	removeFrame();
	var pageFrame=document.createElement('iframe');
	pageFrame.classList='hiddenFrame';
	pageFrame.style.display='none';
	pageFrame.src='search.php?page='+page+'&search='+search;
	document.body.appendChild(pageFrame);
}
function nothing(){
	return document.querySelector('.tr')==undefined?true:false;
}
function outLoader(){
	var popup=document.querySelector('.popup-fade');
	popup.classList+=' fadeOut';
	popup.style.opacity=0;
	setTimeout(function(){popup.style.display='none';},900);
}
function showLoader(){
	var popup=document.querySelector('.popup-fade');
	popup.style.display='block';
	popup.classList='popup-fade fade';
	popup.style.opacity=1;
}
function remove(id,height){
	document.getElementById('trash_'+id).onclick='';
	var opacity=1;
	var tr=document.getElementById(id);
	var timer=setInterval(function(){
		if(opacity<=0){
			clearInterval(timer);
			tr.style.height=height+'px';
			tr.innerHTML='';
			timer=setInterval(function(){
				if(height<=0){
					clearInterval(timer);
					tr.remove();
					if(nothing()==true){
						var active=document.getElementById('active');
						if(active!=undefined){
							if(parseInt(active.innerHTML)!=1){
								var openPage=parseInt(active.innerHTML)-1;
								document.getElementById('page'+openPage).click();
							}else{
								var openPage=parseInt(active.innerHTML)+1;
								document.getElementById('page'+openPage).click();
							}
						}else{
							document.getElementById('svgIframe').src='nothing.php';
							document.getElementById('trSvgIframe').classList='trSvgIframeBlock';
						}
					}
				}
				tr.style.height=height+'px';
				height=height-1;
			},1);
		}
		tr.style.opacity=opacity;
		opacity=opacity-0.1;
	},10);
	removeFrame();
	var frame=document.createElement("iframe");
	frame.classList='hiddenFrame';
	frame.src="remove.php?id="+id;
	frame.style.display="none";
	document.body.appendChild(frame);
}
function slideDown(){
	var tr=document.getElementsByClassName('tr');
	var i=0;
	function tableRowsDown(){ 
		setTimeout(function(){
			tr[i].classList='tr fade';
			i++;
			if(i<tr.length){
				tableRowsDown();
			}else{
				document.getElementById('pagination').classList='fade';
				outLoader();
				document.getElementById('searchInput').focus();
			}
		},50);
	}
	tableRowsDown();
}
window.onload=function(){
	var th=document.getElementsByClassName('th');
	var i=0;
	function tableHead(){ 
		setTimeout(function(){
			th[i].classList='th fade';
			i++;
			if(i<th.length){
				tableHead();
			}else{
				if(nothing()==true){
					document.getElementById('svgIframe').src='nothing.php';
					document.getElementById('trSvgIframe').classList='trSvgIframeBlock';
					outLoader();
				}else{
					slideDown();
				}
			}
		},50) 
	}
	tableHead();
}