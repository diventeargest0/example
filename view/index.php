<?if(!$_COOKIE['auth']):?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Авторизация</title>
	<link rel="icon" type="image/png" href="favicon.ico">
	<link rel="stylesheet" type="text/css" href="auth/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="auth/css/util.css">
	<link rel="stylesheet" type="text/css" href="auth/css/main.css">
<style>
	.none{
        display: none;
	}
    .popupHidden {
		color: #fff;
        animation: fadeOut 1s;
        width: 300px;
        text-align: center;
        border-radius: 60px;
        padding: 15px 0px;
        position: fixed;
        right: 20px;
        top: 20px;
        z-index: 9999;
    }
    .popupVisible {
		color: #fff;
        animation: fadeIn 1s;
        width: 300px;
        background-color: #000;
        text-align: center;
        border-radius: 60px;
        padding: 15px 0px;
        position: fixed;
        right: 20px;
        top: 20px;
        z-index: 9999;
    }
    @keyframes fadeIn {
        from {opacity: 0;}
        to {opacity: 1;}
    }
    @keyframes fadeOut {
        from {opacity: 1;}
        to {opacity: 0;}
    }
</style>
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-b-160 p-t-50">
				<form action="auth/auth.php" method="post" target="auth" class="login100-form validate-form" onsubmit="document.querySelector('.rs1').classList='wrap-input100 rs1 rs2 validate-input';if(document.querySelector('.input100').value.trim()!='')var popup=document.getElementById('popup');popup.innerHTML='Проверка...';popup.style.backgroundColor='rgba(0,0,0,.7)';popup.classList='popupVisible';">
					<span class="login100-form-title p-b-43">введите пароль доступа</span>
					<div class="wrap-input100 rs1 rs2 validate-input" data-validate="Извините, но без пароля никак">
						<input class="input100" type="text" name="pass" autocomplete="off" >
						<span class="label-input100">Пароль</span>
					</div>
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							войти
						</button>
					</div>
				</form>
			</div>
			<iframe style="display:none;" name='auth'></iframe>
		</div>
	</div>
	<div class="none" id="popup"></div>
	<script src="auth/js/jquery-3.2.1.min.js"></script>
	<script src="auth/js/main.js"></script>
</body>
</html>
<?else:?>
<!DOCTYPE html>
<html>
<head>
<title>ЗАЯВКИ</title>
<link rel="icon" type="image/png" href="favicon.ico">
<link rel='stylesheet' href='style.css?ver=0001'>
<script type='text/javascript' src='script.js?ver=003'></script>
</head>
<body>
<?$DB_connection=mysqli_connect('DBHOST','DBLOGIN','DBPASSWORD','DBNAME');
$countRequest=mysqli_query($DB_connection,'select count(id) as count from bid;');
$count=$countRequest->fetch_array();
$num=13;
$page=$_GET['page'];
$total=intval(($count['count']-1)/$num)+1;
$page=intval($page);
if(empty($page) or $page<0)$page=1;
if($page>$total) $page=$total;
$start=$page*$num-$num;
if($page>4)$leftDots='<big id="page'.round($page/2-0.1).'" onclick="search('.round($page/2-0.1).',``)">...</big>';
if($page<$total-3)$rightDots='<big id="page'.round(($total+$page+1)/2).'" onclick="search('.round(($total+$page+1)/2).',``)">...</big>';
if($page>3)$pervpage='<big id="page1" onclick="search(1,``)">1</big>'.$leftDots;
if($page<$total-2)$nextpage=$rightDots.'<big id="page'.$total.'" onclick="search('.$total.',``)">'.$total.'</big>';
if($page-2>0)$page2left='<big id="page'.($page-2).'" onclick="search('.($page-2).',``)">'.($page-2).'</big>';
if($page-1>0)$page1left='<big id="page'.($page-1).'" onclick="search('.($page-1).',``)">'.($page-1).'</big>';
if($page+2<=$total)$page2right='<big id="page'.($page+2).'" onclick="search('.($page+2).',``)">'.($page+2).'</big>';
if($page+1<=$total)$page1right='<big id="page'.($page+1).'" onclick="search('.($page+1).',``)">'.($page+1).'</big>';
if($total>1){$pagination='<p>'.$pervpage.$page2left.$page1left.'<big id="active">'.$page.'</big>'.$page1right.$page2right.$nextpage.'</p>';}
$DB_query=mysqli_query($DB_connection,'select * from bid limit '.$start.','.$num.';');
?>
<br>
<div id="view" class="fade"></div>
<h4 id="pagination" class="h4"><?=$pagination?></h4>
<form class="form" action="search.php" target="iFrameName" onsubmit="showLoader()" method="get">
  <input id="searchInput" class="input" name="search" type="text" autocomplete="off" placeholder="Поиск...">
  <button class="button" type="submit"></button>
</form>
<iframe id='iFrameID' name='iFrameName'></iframe>
<br><br>
<table class='table' id='table'>
<tr>
  <col width="5%">
  <col width="9%">
  <col width="10%">
  <col width="10%">
  <col width="5%">
  <col width="5%">
  <col width="16%">
  <col width="10%">
  <col width="16%">
  <col width="10%">
  <col width="4%">
  <th class='th thOpacity'>Дата</th>
  <th class='th thOpacity'>IP-адрес</th>
  <th class='th thOpacity'>Смена</th>
  <th class='th thOpacity'>ФИО ребёнка</th>
  <th class='th thOpacity'>Дата рождения ребёнка</th>
  <th class='th thOpacity'>Кол-во полных лет</th>
  <th class='th thOpacity'>ФИО родителя</th>
  <th class='th thOpacity'>Телефон родителя</th>
  <th class='th thOpacity'>Адрес проживания</th>
  <th class='th thOpacity'>E-mail</th>
  <th class='th thOpacity'>&#9888;</th>
</tr>
<tr id='trSvgIframe' class='trSvgIframeNone'>
  <td colspan=11>
    <iframe id='svgIframe' width='100%' height='190px' frameborder='no' src=''></iframe>
  </td>
</tr>
<?
while($response=mysqli_fetch_array($DB_query)):
?>
<tr id="<?=$response['id']?>" class='tr trOpacity'>
  <td class='td' ondblclick="view(<?=$response['id']?>)" title="<?=$response['ts']?>"><?=$response['ts']?></td>
  <td class='td' ondblclick="view(<?=$response['id']?>)" title="<?=$response['ip']?>"><?=$response['ip']?></td>
  <td class='td' ondblclick="view(<?=$response['id']?>)" title="<?=$response['watch']?>"><?=$response['watch']?></td>
  <td class='td' ondblclick="view(<?=$response['id']?>)" title="<?=$response['child']?>"><?=$response['child']?></td>
  <td class='td' ondblclick="view(<?=$response['id']?>)" title="<?=$response['birth']?>"><?=$response['birth']?></td>
  <td class='td' ondblclick="view(<?=$response['id']?>)" title="<?=$response['age']?>"><?=$response['age']?></td>
  <td class='td' ondblclick="view(<?=$response['id']?>)" title="<?=$response['parent']?>"><?=$response['parent']?></td>
  <td class='td' ondblclick="view(<?=$response['id']?>)" title="<?=$response['phone']?>"><?=$response['phone']?></td>
  <td class='td' ondblclick="view(<?=$response['id']?>)" title="<?=$response['address']?>"><?=$response['address']?></td>
  <td class='td' ondblclick="view(<?=$response['id']?>)" title="<?=$response['email']?>"><?=$response['email']?></td>
  <td id='trash_<?=$response["id"]?>' title="Удалить" class='td trash' onclick="remove(<?=$response['id']?>,this.offsetHeight)"><img src="trash.ico" width="20px"></td>
</tr>
<?endwhile;?>
</table>
<div class="popup-fade">
	<div class="loader">
	  <div class="inner one"></div>
	  <div class="inner two"></div>
	  <div class="inner three"></div>
	</div>
</div>
<?mysqli_close($DB_connection);?>
</body>
</html>
<?endif?>